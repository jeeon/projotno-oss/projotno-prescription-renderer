try {
  var QRCode = require('qrcode');
  var _ = require('lodash');
  var moment = require('moment');
  var rasterizeHTML = require('rasterizehtml');
  var jsPDF = require('jspdf');
} catch (e) {
  // do nothing
}

var ProjotnoPrescriptionRenderer = (function () {
  return {
    render: _render
  };

  function _render(template, prescription, orientation, outputMode, ratio, successCallback, errorCallback) {
    if (orientation == 'landscape' || orientation == 'l') orientation = 'l';
    else if (orientation == 'portrait' || orientation == 'p') orientation = 'p';
    else {
      if (errorCallback) errorCallback('INVALID_ORIENTATION');
      else console.error('ProjotnoPrescriptionRenderer: INVALID_ORIENTATION');
      return;
    }

    if (ratio && ratio <= 0) {
      if (errorCallback) errorCallback('INVALID_RATIO');
      else console.error('ProjotnoPrescriptionRenderer: INVALID_RATIO');
      return;
    }

    // 28:21
    var w = 1206;
    var h = 905;

    if (orientation == 'p') {
      var tmp = w;
      w = h;
      h = tmp;
    }

    var canvas = document.createElement('canvas');
    var ctx = canvas.getContext('2d');

    var r = ratio;
    if (!r) {
      var dpr = window.devicePixelRatio || 1;
      var bsr = ctx.webkitBackingStorePixelRatio ||
        ctx.mozBackingStorePixelRatio ||
        ctx.msBackingStorePixelRatio ||
        ctx.oBackingStorePixelRatio ||
        ctx.backingStorePixelRatio || 1;

      var r = dpr / bsr;
      r = r == 1 ? 3 : r;
    }

    canvas.width = w * r;
    canvas.height = h * r;
    canvas.style.width = w + 'px';
    canvas.style.height = h + 'px';

    ctx.scale(r, r);
    if (orientation == 'p') {
      ctx.translate(w, 0);
      ctx.rotate(Math.PI / 2);
    }

    _prepare(prescription, function (data) {
      var tmpl;
      try {
        tmpl = template(data);
      } catch (e) {
        if (errorCallback) errorCallback('INVALID_TEMPLATE: ' + e);
        else console.error('ProjotnoPrescriptionRenderer: INVALID_TEMPLATE');
        return;
      }
      rasterizeHTML.drawHTML(tmpl, canvas)
        .then(function () {
          var image = canvas.toDataURL('image/png');
          if (outputMode == 'image/datauri') {
            successCallback(image);
          } else if (outputMode == 'image/data') {
            successCallback(image.split(',')[1]);
          } else if (outputMode == 'image/window') {
            window.open(image, '_blank');
            successCallback();
          } else if (outputMode == 'pdf/datauri') {
            successCallback(generatePdf(orientation, image).output('datauristring'));
          } else if (outputMode == 'pdf/data') {
            successCallback(generatePdf(orientation, image).output('datauristring').split(',')[1]);
          } else if (outputMode == 'pdf/window') {
            generatePdf(orientation, image).output('dataurlnewwindow');
            successCallback();
          } else if (outputMode == 'pdf/print') {
            var pdf = generatePdf(orientation, image);
            pdf.autoPrint();
            pdf.output('dataurlnewwindow');
            successCallback();
          } else {
            if (errorCallback) errorCallback('INVALID_OUTPUTMODE');
            else console.error('ProjotnoPrescriptionRenderer: INVALID_OUTPUTMODE');
          }
        });
    });

  }

  function generatePdf(orientation, image) {
    // projotno letter-head paper size = 28cm x 21cm
    var pdf = new jsPDF(orientation, 'cm', orientation === 'l' ? [28, 21] : [21, 28]);
    pdf.addImage(image, 'PNG',
      0, 0, pdf.internal.pageSize.width, pdf.internal.pageSize.height,
      undefined, 'FAST');
    return pdf;
  }

  function _formatDate(date) {
    return date ? moment(date).format('MMM D, YYYY') : '';
  }

  function _calcAge(dob) {
    return dob ? moment(dob).fromNow(true) : '';
  }

  function _formatDose(d) {
    if (!d || d.trim().length === 0) return d;
    var slashPos = d.indexOf('/');
    if (slashPos === -1) return d;

    var nom = d.substring(0, slashPos);
    var denom = d.substring(slashPos + 1);

    return '<sup>' + nom + '</sup>\u2044<sub>' + denom + '</sub>';
  }

  function _prepare(prescription, callback) {
    var data = {};

    // vitals (left-side)
    data.vitals = [];
    if (prescription.patients_id && prescription.patients_id.dob) {
      data.vitals.push({
        name: 'Age',
        value: _calcAge(prescription.patients_id.dob)
      });
    }
    if (prescription.appointment_id) {
      if (prescription.appointment_id.patient_weight) {
        data.vitals.push({
          name: 'Weight',
          value: prescription.appointment_id.patient_weight + ' kg'
        });
      }
      if (prescription.appointment_id.patient_pulse) {
        data.vitals.push({
          name: 'Pulse',
          value: prescription.appointment_id.patient_pulse + ' bpm'
        });
      }
      if (prescription.appointment_id.patient_systole && prescription.appointment_id.patient_diastole) {
        data.vitals.push({
          name: 'Blood Pressure',
          value: prescription.appointment_id.patient_systole + '/' + prescription.appointment_id.patient_diastole + ' mmHg'
        });
      }
      if (prescription.appointment_id.patient_temp) {
        data.vitals.push({
          name: 'Temperature', value:
          prescription.appointment_id.patient_temp + ' &deg;F'
        });
      }
      if (prescription.appointment_id.patient_glucose) {
        data.vitals.push({
          name: 'Blood Sugar',
          value: prescription.appointment_id.patient_glucose + ' mmol/L'
        });
      }
      if (prescription.appointment_id.patient_respiratoryrate) {
        data.vitals.push({
          name: 'Respiration',
          value: prescription.appointment_id.patient_respiratoryrate
        });
      }
    }

    // complaints (left-side)
    data.complaints = prescription.encodedSymptoms ? _.map(prescription.encodedSymptoms, function (s) {
      var details = '';
      var a = s.complainDetail || '';
      var b = (s.complainValue || '') + ' ' +
        (s.complainDur || '') + ' ' +
        (s.situation && s.situation.length > 0 && s.situation[0].primaryTerm ? s.situation[0].primaryTerm : '');

      details = a + (b.trim().length == 0 ? '' : ', ' + b);
      return {
        complain: s.complainText[0].primaryTerm,
        details: details.trim().length == 0 ? '' : details
      };
    }) : _.map(prescription.chief_complaint, function (s) {
      var details = '';
      var a = s.complainDetail || '';
      var b = (s.complainValue || '') + ' ' +
        (s.complainDur || '') + ' ' +
        (s.situation && s.situation.length > 0 && s.situation[0].primaryTerm ? s.situation[0].primaryTerm : '');

      details = a + (b.trim().length == 0 ? '' : ', ' + b);
      if (typeof s.complainText == 'string') {
        return {
          complain: s.complainText,
          details: details.trim().length == 0 ? '' : details
        };
      } else {
        return {
          complain: s.complainText[0].primaryTerm,
          details: details.trim().length == 0 ? '' : details
        };
      }
    });

    // diagnosis (left-side)
    data.diagnosis = prescription.diagnoses && _.keys(prescription.diagnoses).length > 0 ? [
      {
        type: 'Provisional',
        data: prescription.diagnoses.provisional ? _.map(prescription.diagnoses.provisional, function (d) {
          return d.primaryTerm;
        }) : []
      },
      {
        type: 'Final',
        data: prescription.diagnoses.final ? _.map(prescription.diagnoses.final, function (d) {
          return d.primaryTerm;
        }) : []
      }
    ] : [];

    //History (left-side)
    data.history = prescription ? [
      {
        type: 'Personal',
        data: prescription.personalinfo ? _.map(prescription.personalinfo, function (d) {
          return d._id;
        }) : []
      },
      {
        type: 'Family',
        data: prescription.familyinfo ? _.map(prescription.familyinfo, function (d) {
          return d._id;
        }) : []
      },
      {
        type: 'Chronic',
        data: prescription.chronicinfo ? _.map(prescription.chronicinfo, function (d) {
          return d._id;
        }) : []
      },
      {
        type: 'Drug',
        data: prescription.druginfo ? _.map(prescription.druginfo, function (d) {
          return d._id;
        }) : []
      }
    ] : [];


    // advice (left-side)
    // FIXME: remove v3 free-text handling
    data.advices = prescription.advices && prescription.advices.length > 0 ?
      typeof prescription.advices[0] == 'string' ?
        prescription.advices : _.map(prescription.advices, function (a) { return a.primaryTerm; })
      : [];

    // meta (top-side)
    data.appointmentId = prescription.appointment_id._id;
    data.fee = (prescription.fee || 0) + ' BDT';
    data.followupDate = _formatDate(prescription.followup_date);
    data.porijonName = prescription.patients_id.fullname || '';
    data.sbName = prescription.rmp_id.fullname || '';
    data.sbContact = prescription.rmp_id.phone || '';
    data.date = _formatDate(prescription.created_at);

    // Rx
    data.instructions = prescription.instruction ? _.map(prescription.instruction, function (ins) {
      var duration = (ins.duration || '') + ' ' + (ins.duration_type || '');
      var doses = _.join(_.map(_.values(ins.doses), _formatDose), ' + ');
      return ins.selected[0].trade_name
        + (doses.trim().length === 0 ? '' : ' - ')
        + doses
        + (duration.trim().length === 0 ? '' : ' ----- ')
        + duration
        + _.join(_.map(ins.instructions, function (i) {
          return ' - ' + i.text;
        }), ', ')
        + _.join(_.map(ins.additional_instructions, function (a) {
          return ' - ' + a.text;
        }), ', ');
    }) : [];

    // investigations (bottom-side)
    // FIXME: remove v3 free-text handling
    data.investigations = prescription.investigations && prescription.investigations.length > 0 ?
      typeof prescription.investigations[0] == 'string' ?
        prescription.investigations : _.map(prescription.investigations, function (v) { return v.primaryTerm; })
      : [];

    // doctor (bottom-side)
    data.doctorSignature = 'data:image/png;base64,' + prescription.doctors_id.signature_url;
    data.doctorName = prescription.doctors_id.fullname;
    data.doctorDetails = prescription.doctors_id.identity;

    // porijon id qrcode (top-side)
    var qrConfig = { width: 90, height: 90, margin: 0 };
    if (QRCode.toDataURL) { // npm
      QRCode.toDataURL(prescription.patients_id.serialnumber, qrConfig, function (err, url) {
        data.qrcode = url;
        callback(data);
      });
    } else { // bower
      var qrcode = new QRCode(document.createElement('div'), qrConfig);
      qrcode._el.children[1].onload = function () {
        data.qrcode = qrcode._el.children[1].src;
        callback(data);
      };
      qrcode.makeCode(prescription.patients_id.serialnumber);
    }
  }
})();

try {
  module.exports = ProjotnoPrescriptionRenderer;
} catch (e) {
  // do nothing
}
