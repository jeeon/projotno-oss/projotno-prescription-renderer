module.exports = {
  module: {
    rules: [
      { test: /\.handlebars$/, loader: "handlebars-loader" }
    ]
  },
  entry: './test-npm.js',
  output: {
    filename: 'bundle.js',
    path: __dirname + '/dist'
  }
}
